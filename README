Application description:
The HTML application  that displays interesting Chuck Norris jokes from API[https://api.chucknorris.io/],
created running in a container,
provided Dockerfile which  build the image of the source code and NGINX installed.

Used alpine-linux  as base image from Docker Hub.  A webserver nginx  deployed as proxy.

When you click on the chuck norris image the joke/fact in te below box changes.

Setup:
Run the following command to build the docker image of source code:
docker build -t chucknorisfacts -f Dockerfile .
Here, -t  used to assign a name-tag to the docker images.

Check if the docker image is built successfully,
docker images

Use the same image to create a running container:
docker run -d --name <container-name> chucknorisfacts
where;
-d  used to run a container in background, print container ID.
--name used to assign a name to the container.

docker-compose up
open in the browser "http://localhost:8000"

Automation with Jenkins with Ansible:

# Deploy on a docker container using Ansible
# *Jenkins Job name:* `Deploy_on_Container_using_ansible`

### Pre-requisites

1. Jenkins server
2. Docker-host server
3. Ansible server

1. Create `create-docker-image.yml` under *`/opt/docker`*
   ```sh
   ---
   - hosts: all
     #ansadmin doesn't need root access to create an image
     become: true

     tasks:
     - name: building docker image
       command: "docker build -t chucknorisfacts ."
       args:
         chdir: /opt/docker
   ```

### Integration between Ansible-control-node and Jenkins

Install "publish Over SSH"
 - `Manage Jenkins` > `Manage Plugins` > `Available` > `Publish over SSH`

Enable connection between Ansible-control-node and Jenkins

- `Manage Jenkins` > `Configure System` > `Publish Over SSH` > `SSH Servers`

	- SSH Servers:
                - Name: `ansible-server`
		- Hostname:`<ServerIP>`
		- username: `ansadmin`

       -  `Advanced` > choose `Use password authentication, or use a different key`
		 - password: `*******`

### Steps to create "Deploy_on_Container_using_ansible" Jenkin job
#### From Jenkins home page select "New Item"
   - Enter an item name: `Deploy_on_Container_using_ansible`

   - *Source Code Management:*
      - Repository: `https://ppatil1989@bitbucket.org/ppatil1989/assignment.git`
      - Branches to build : `*/master`
   - *Poll SCM* :      - `* * * *`


 - *Post-build Actions*
   - Send build artifacts over SSH
     - *SSH Publishers*
      - SSH Server Name: `ansible-server`
       - `Transfers` >  `Transfer set`
            - Source files: `webapp/target/*.war`
	       - Remove prefix: `webapp/target`
	       - Remote directory: `//opt//docker`
	       - Exec command:
                ```sh
                ansible-playbook -i /opt/docker/hosts /opt/docker/create-docker-image.yml;
                ```

Save and run the job now.

## Integration Kubernetes with Jenkins

# *Jenkins CI Job:* `Deploy_on_Kubernetes-CI`

### Pre-requisites

1. Jenkins server 
1. Ansible server
1. Kubernetes cluster
 
### Steps to create "Deploy_on_Kubernetes_CI" Jenkin job
#### From Jenkins home page select "New Item"
   - Enter an item name: `Deploy_on_Kubernetes_CI`
     - Copy from: `Deploy_on_Docker_Container_using_Ansible_playbooks`
     
   - *Source Code Management:*
      - Repository: `https://ppatil1989@bitbucket.org/ppatil1989/assignment.git`
      - Branches to build : `*/master`  
   - *Poll SCM* :      - `* * * *`


 - *Post-build Actions*
   - Send build artifacts over SSH
     - *SSH Publishers*
      - SSH Server Name: `ansible-server`
       - `Transfers` >  `Transfer set`
           - Source files: `webapp/target/*.war`
	       - Remove prefix: `webapp/target`
	       - Remote directory: `//opt//docker`
	       - Exec command: 
                ```sh 
                ansible-playbook -i /opt/docker/hosts /opt/docker/create-simple-devops-image.yml --limit localhost;
                ```

Save and run the job.

# *Jenkins CD Job:* `Deploy_on_Kubernetes-CD`

### Steps to create "Deploy_on_Kubernetes_CI" Jenkin job
#### From Jenkins home page select "New Item"
   - Enter an item name: `Deploy_on_Kubernetes_CI`
     - Freestyle Project
	 
  - *Post-build Actions*  
    - Send build artifacts over SSH  
      - *SSH Publishers*  
	       - Exec command: 
                ```sh 
                ansible-playbook -i /opt/docker/hosts /opt/docker/kubernetes-valaxy-deployment.yml;
                ansible-playbook -i /opt/docker/hosts /opt/docker/kubernetes-valaxy-service.yml;
                ```
Save and run the job.

# Integrating Kubernetes cluster with Ansible

1. Login to ansible server and copy public key onto kubernetes cluseter master account 

2. Update hosts file with new group called kubernetes and add kubernetes master in that. 
		
3.  Check for pods, deployments and services on kubernetes master
    ```sh 
    kubectl get pods -o wide 
    kubectl get deploy -o wide
    kubectl get service -o wide
    ```
	
1. Access application suing service IP
   ```sh
   wget <kubernetes-Master-IP>:31200```

