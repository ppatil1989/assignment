FROM alpine:latest

#Update package manager
RUN apk update
RUN apk add --update-cache
RUN rm -rf /var/cache/apk/*

#Installing Nginx Package
RUN apk add nginx
RUN apk add --update nginx

#Copy
COPY ./src/index.html  /usr/share/nginx/html

EXPOSE 8000
CMD ["nginx", "-g", "daemon off;"]